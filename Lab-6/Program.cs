﻿using System;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        int totalTasks = 10;

        Console.WriteLine("Processing tasks sequentially:");
        Sequentially(totalTasks);

        Console.WriteLine("\nProcessing tasks simultaneously using threads:");
        Concurrently(totalTasks);

        Console.WriteLine("\nPress any key to exit...");
        Console.ReadKey();
    }

    static void Sequentially(int totalTasks)
    {
        for (int i = 0; i < totalTasks; i++)
        {
            Console.WriteLine($"Processing task {i + 1}...");
            Thread.Sleep(1000); 
        }
    }

    static void Concurrently(int totalTasks)
    {
        int numThreads = 5; 

        Thread[] threads = new Thread[numThreads];

        // Divide o trabalho em lotes para as threads
        int batchSize = totalTasks / numThreads;
        int remainingTasks = totalTasks % numThreads;

        int startIndex = 0;
        for (int i = 0; i < numThreads; i++)
        {
            int batchSizeForThread = batchSize + (i < remainingTasks ? 1 : 0);
            int endIndex = startIndex + batchSizeForThread;

            threads[i] = new Thread(() => TaskByLots(startIndex, endIndex));
            threads[i].Start();

            startIndex = endIndex;
        }
        
        foreach (var thread in threads)
        {
            thread.Join();
        }
    }

    static void TaskByLots(int start, int end)
    {
        for (int i = start; i < end; i++)
        {
            Console.WriteLine($"Processing task {i + 1}...");
            Thread.Sleep(1000); 
        }
    }
}